FROM ubuntu:18.04

RUN apt-get update && \
      apt-get -y install sudo

RUN useradd -m worker && echo "worker:worker" | chpasswd && adduser worker sudo

USER worker
CMD /bin/bash
