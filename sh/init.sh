#!/usr/bin/env bash

. ./defs.sh

# CHMODING
chmod -R +x .

# Commands
for DIR in */;
do
	echo ""
	"./$DIR""_init.sh" && print_success "$DIR"
done 

