#!/usr/bin/env bash
PROGRAM=docker
print_installing "$PROGRAM"

TMP_LOCATION=/tmp/install_docker.sh

wget get.docker.com -O "$TMP_LOCATION"
sh "$TMP_LOCATION" && print_success "$PROGRAM"

echo -e ">>> Should I add $USER to docker group? (It is required to run docker without sudo)"
read ADD_TO_DOCKER_GROUP

if ! [ -z "$ADD_TO_DOCKER_GROUP" ]; then
    sudo usermod -aG docker "$USER" && print_success "$PROGRAM" "$YELLOW $USER""$GREEN added to docker group!"
else
    print_failure "$PROGRAM" "Not adding to docker group.."
fi
