#!/usr/bin/env bash
PROGRAM=COMMANDS
print_installing "$PROGRAM"

declare -a SCRIPTS

SCRIPTS=( composer portainer )

for SCRIPT in "${SCRIPTS[@]}";
do
	echo ""
	echo -e "	>>$YELLOW [$SCRIPT]$CLOSE Starting..."
	`dirname "$0"`"/""$SCRIPT"".sh" && print_success "$SCRIPT" || print_failure "$SCRIPT"
done
